node.set["apache"]["user"]  = "vagrant"
node.set["apache"]["group"] = "vagrant"

node.set['mysql']['server_root_password'] = "root"
node.set['mysql']['server_debian_password'] = "root"
node.set['mysql']['server_repl_password'] = "root"

node.set['mysql']['bind_address'] = '0.0.0.0'
node.set['mysql']['allow_remote_root'] = "1";

include_recipe "apt"

apt_repository "dotdeb-php54" do
  uri "http://packages.dotdeb.org"
  distribution "squeeze-php54"
  components ["all"]
  key "http://www.dotdeb.org/dotdeb.gpg"
end

include_recipe "apache2"
include_recipe "apache2::mod_php5"
include_recipe "apache2::mod_rewrite"
include_recipe "apache2::mod_ssl"

include_recipe "php"

include_recipe "mysql::server"

package "git-core"
package "imagemagick"

package "php5-cli"
package "php5-curl"
package "php5-gd"
package "php5-dev"
package "php5-mysql"
package "php5-imagick"

execute "disable-default-site" do
  command "a2dissite default"
end

node[:app][:web_apps].each do |identifier, app|
  web_app identifier do
    server_name app[:server_name]
    server_aliases app[:server_aliases]
    docroot app[:host_project_folder]
    php_timezone app[:php_timezone]
  end
end

