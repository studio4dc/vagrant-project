# Vagrant LAMP stack for Projects
This is a work in progress, but a basic setup is ready now.

## Requirements:
* [VirtualBox](https://www.virtualbox.org)
* [Vagrant](http://vagrantup.com)
* [VagrantOmnibus](https://github.com/schisamo/vagrant-omnibus)

## Installation
Clone this repository
     
    $ git clone git@bitbucket.org:studio4dc/vagrant-project.git PROJECT
    $ cd PROJECT
    $ vagrant plugin install vagrant-omnibus
    $ cp Vagrantfile.dist Vagrantfile
    $ rm -rf .git

## Usage
Start VM

	$ change configuration in Vagrantfile
	$ add domains to your hosts file
    $ vagrant up
    
You can now access your project at [http://*](http://*)

## Installed software:
* Apache
* MySQL
* php

## TODO